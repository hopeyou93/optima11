<?php


namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use Illuminate\Support\Facades\Mail;


class ContactController extends Controller
{
    public function contact(Request $request)
    {


        $name = $request->input('name');
        $email = $request->input('email');
        $subject = $request->input('subject');
        $message = $request->input('message');



        $bag = compact('name', 'email', 'subject', 'message');

        //logica della mail

        $emails = new ContactMail($bag); //email

        Mail::to('Presto@presto.it')->send($emails);

       return redirect(route('workus'))->with('message', 'Messaggio inviato con successo');
    }
}
