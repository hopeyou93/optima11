<?php


namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Article;
use App\Models\Favourites;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class FrontendController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = User::all();
        $articles = Article::where('published', true)->get();
        return view('home', compact('articles'));
    }
  /*   public function articles()
    {
        $articles = Article::where('published', true)->get();
        return view('articles', compact('articles'));
    } */

    public function whoweare()
    {
        return view('whoweare');
    }

    public function workus()
    {
        return view('workus');
    }


    public function blogNews()
    {
        $articles = Article::where('published', true)->get();
        return view('articles', compact('articles'));
    }

    public function article($article_id, Favourites $favourites)
    {   
        $article = Article::where('published', true)->where('id', $article_id)->first();

        $favCount = Favourites::where('article_id', $article_id)->pluck('id');

        if (Auth::user() != null) {
            $favourites = Favourites::where('article_id', $article_id)->where('user_id', Auth::user()->id)->first();
        } else {
            $favourites = Favourites::where('article_id', $article_id)->first();
        }
    
        if($article) {
            $relatedarticles = Article::where('id', '!=', $article->id)->get();
            return view('article', compact('article', 'relatedarticles', 'favourites', 'favCount'));
        } else {
            return redirect('/blog');
        }
    }


    public function search(Request $request)
    {

       $q = $request->input('q');
       $articles = Article::search($q)->where('published', true)->get();

       return view('search', compact('q', 'articles'));
    }


    public function userpage(User $users)
    {

        $articles = Article::where('user_id', $users->id)->get();
        return view('userpage', compact('users', 'articles'));
    }

    public function favourites(Article $article)
    {
        $i = new Favourites();
        $i->favourite=true;
        $i->user_id=Auth::user()->id;
        $i->article_id=$article->id;
        $i->save();

        return back();
    }

    public function removeFavourites(Favourites $favourites)
    {

        $favourites->delete();
        return back();
    }

}
