<?php

namespace App\Providers;

use Laravel\Nova\Nova;
use Aulab\Dashboard\Dashboard;
use Illuminate\Support\Facades\Gate;
use Laravel\Nova\NovaApplicationServiceProvider;
use EricLagarda\NovaGallery\NovaGallery;
use OptimistDigital\MediaField\NovaMediaLibrary;


class NovaServiceProvider extends NovaApplicationServiceProvider
{






    public function boot()
    {
        parent::boot();
    }

    /**
    * Register the Nova routes.
    *
    * @return void
    */
    protected function routes()
    {
        Nova::routes()
        ->withAuthenticationRoutes()
        ->withPasswordResetRoutes()
        ->register();
    }

    /**
    * Register the Nova gate.
    *
    * This gate determines who can access Nova in non-local environments.
    *
    * @return void
    */
    protected function gate()
    {
        Gate::define('viewNova', function ($user) {
            return $user->isAdmin();
        });
    }

    /**
    * Get the cards that should be displayed on the default Nova dashboard.
    *
    * @return array
    */
    protected function cards()
    {
        return [
            new Dashboard,
        ];
    }

    /**
    * Get the extra dashboards that should be displayed on the Nova dashboard.
    *
    * @return array
    */
    protected function dashboards()
    {
        return [];
    }

    /**
    * Get the tools that should be listed in the Nova sidebar.
    *
    * @return array
    */


    public function tools()
    {
        return [
            new \OptimistDigital\MediaField\NovaMediaLibrary,
        ];
    }

    /**
    * Register any application services.
    *
    * @return void
    */
    public function register()
    {
        //
    }




}
