<?php

namespace App\Nova;

use App\Models\User;
use Eminiarts\Tabs\Tabs;

use Spatie\TagsField\Tags;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Waynestate\Nova\CKEditor;
use Eminiarts\Tabs\TabsOnEdit;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Boolean;


use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\BelongsTo;
use Jackabox\DuplicateField\DuplicateField;
use Laravel\Nova\Http\Requests\NovaRequest;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use DmitryBubyakin\NovaMedialibraryField\Fields\Medialibrary;

class Article extends Resource
{
    use TabsOnEdit;
    /**
    * The model the resource corresponds to.
    *
    * @var string
    */
    public static $model = \App\Models\Article::class;

    public static $group = 'Contenuti';

    public static function label() {
        return __('Articles');


    }


    public static function singularLabel() {
        return __('Article');
    }



    /**
    * The single value that should be used to represent the resource when being displayed.
    *
    * @var string
    */
    public static $title = 'title';

    /**
    * The columns that should be searched.
    *
    * @var array
    */

/*     public static function indexQuery(NovaRequest $request, $query)
    {
        return $query->where('user_id', $request->user()->id);
    } */


    public static $search = [
        'id', 'title', 'description'
    ];

    /**
    * Get the fields displayed by the resource.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return array
    */
    public function fields(Request $request)
    {
        return [

            (new Tabs('Tabs', [

                'Info'  => [
                    ID::make(__('ID'), 'id')->sortable(),

                    Text::make(__('Titolo'), 'title')
                    ->rules('required')
                    ->sortable(),

                    BelongsTo::make('User'),

                    Number::make(__('Prezzo'), 'prezzo')
                    ->min(1)->max(1000)->step(0.01)
                    ->rules('required'),

                    Text::make(__('Marca'), 'marca')
                    ->rules('required')
                    ->sortable(),
                    Text::make(__('Taglia'), 'taglia')
                    ->rules('required')
                    ->sortable(),
                    Text::make(__('Condizioni'), 'condizioni')
                    ->rules('required')
                    ->sortable(),
                    Text::make(__('Colore'), 'colore')
                    ->rules('required')
                    ->sortable(),
                    Text::make(__('Locazione'), 'locazione')
                    ->rules('required')
                    ->sortable(),
                    Select::make(__('Pagamento'), 'pagamento')->options([
                        'Carta' => 'Carta di Credito',
                        'Paypal' => 'Paypal',
                    ]),

                    CKEditor::make(__('Descrizione'), 'description')
                    ->hideFromIndex()
                    ->alwaysShow(),

                    DateTime::make(__('Data'), 'created_at')
                    ->format('DD/MM/YYYY')
                    ->sortable(),

                    Boolean::make(__('Pubblicato'), 'published'),


                ],

                'Media' => [
                    Tags::make('Tags'),

                    /* Avatar::make(__('Immagine'), 'img')
                    ->disk('public')
                    ->path('/articoli'), */

                    Images::make('Gallery', 'gallery')
                    ->enableExistingMedia()
                    ->conversionOnIndexView('thumb')
                    ->rules('max:10'),
                ],

                ]))->withToolbar(),

                DuplicateField::make('Duplicate')
                ->withMeta([
                    'resource' => 'articles', // resource url
                    'model' => 'App\Models\Article', // model path
                    'id' => $this->id, // id of record
                    'relations' => [], // an array of any relations to load (nullable).
                    'except' => [], // an array of fields to not replicate (nullable).
                    'override' => ['published' => false] // an array of fields and values which will be set on the modal after duplicating (nullable).
                ])
                ->canSee(function ($request) {
                    return $request->user()->isAdmin();
                }),
        ];
    }

    // public static function indexQuery(NovaRequest $request, $query) {
    //     if($request->user()->isAdmin()) {
    //         return $query;
    //     } else {
    //         return $query->where('user_id', $request->user()->id);
    //     }
    // }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
