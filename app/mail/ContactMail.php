<?php

namespace App\Mail;

use App\Mail\ContactMail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    public $bag;  //busta delle lettere

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($bag)
    {
        $this->bag = $bag;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('sitoWeb@gmail.com')
                    ->view('mail.show');
    }
}
