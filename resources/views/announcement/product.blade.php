<div class="container my-5 py-3">
    <div class="row align-items-center">
        <div class="col-12 col-md-7  text-white mt-5">
            <div class="row justify-content-center">
                <div class="col-7">
                    <div class="single-item">
                        <div>
                            <img  src="./img/iPhone-X-1.png" alt="" class="img-fluid rounded">
                        </div>
                        <div>
                            <img  src="./img/iphone-X-5.png" alt="" class="img-fluid rounded">
                        </div>
                        <div>
                            <img  src="./img/iphone-X-2.png" alt="" class="img-fluid rounded">
                        </div>
                    </div>
                </div>
            </div>    
        </div>
        <div class="col-12 col-md-5 mt-4">
                    <h2 class="text-white">Iphone X</h2>
                    <p class="mt-1 text-white lead">Categoria : Smartphone</p>
                    <p class="mt-1 text-white">Prezzo :  € 350,00</p>
            <div class="accordion" id="accordionExample">
                <div class="card bg-transparent text-white">
                  <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left text-white" type="button" data-toggle="collapse" data-target="#collapseOne" aria-controls="collapseOne">
                        Descrizione
                      </button>
                    </h2>
                  </div>
              
                  <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                      Vendo Iphone X in buone condizioni per passaggio a modello superiore.
                    </div>
                  </div>
                </div>
                <div class="card bg-transparent text-white ">
                  <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed text-white" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Caratteristiche tecniche
                      </button>
                    </h2>
                  </div>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item">Schermo: 5,8” Super Redina Display</li>
                            <li class="list-group-item">CPU: Apple A11 Bionic hexa-core </li>
                            <li class="list-group-item">RAM: 3 GB</li>
                            <li class="list-group-item">Fotocamera posteriore: doppia fotocamera da 12 megapixel</li>
                            <li class="list-group-item">Connettività: nano SIM, Wi‑Fi 802.11ac con tecnologia MIMO</li>
                          </ul>
                    </div>
                  </div>
                </div>
                <div class="card bg-transparent text-white">
                  <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed text-white" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Info Annuncio
                      </button>
                    </h2>
                  </div>
                  <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                     Venduto da <a  class="text-warning" href="./userhost.html">Mario Rossi</a>
                        <address class="mt-1"><i class="fas fa-envelope mr-2"></i> <a class="text-white" href="mailto:sdf">outlook@outlook.com</a></address>
                        <address><i class="fas fa-phone mr-2"></i><a class="text-white" href="telto:sdf"> 333-3333333</a></address>
                    </div>
                  </div>
                </div>
              </div>

            
            
        </div>
         
    </div>
    
</div>


<div class="container-fluid my-3">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-3">
                <h3 class="text-warning font-weight-bold">Annunci Simili</h3>
            </div>  
        </div>  
        <div class="row">
            <div class="col-12">
                <div class="slider">
                    <div class="">
                        <div class="card rounded" style="width: 16rem;">
                            <img class="img-fluid rounded" src="./img/iphone.png" class="card-img-top"  alt="">
                            <div class="card-body">
                                <h5 class="card-title d-inline-block">Iphone 8</h5>
                                <span class="ml-5 px-2"><strong> € 300,00</strong></span>
                                <a href="#" class="btn btn-warning">Vai all'annuncio</a>
                            </div>
                        </div> 
                    </div>
                  
                    <div class="">
                        <div class="card rounded" style="width: 16rem;">
                            <img class="img-fluid rounded" src="./img/airpods.png" class="card-img-top" alt="">
                            <div class="card-body">
                                <h5 class="card-title d-inline-block">Airpods</h5>
                                <span class="ml-5 px-2"><strong> € 150,00</strong></span>
                                <a href="#" class="btn btn-warning">Vai all'annuncio</a>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="card rounded" style="width: 16rem;" >
                            <img class="img-fluid rounded" src="./img/ps4.png" class="card-img-top" alt="">
                            <div class="card-body">
                                <h5 class="card-title d-inline-block">Ps4 500gb</h5>
                                <span class="ml-3 px-2"><strong> € 200,00</strong></span>
                                <a href="#" class="btn btn-warning">Vai all'annuncio</a>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="card rounded" style="width: 16rem;">
                            <img class="img-fluid rounded" src="./img/auto.png" class="card-img-top" alt="">
                            <div class="card-body">
                                <h5 class="card-title d-inline-block">Ford Fiesta</h5>
                                <span class="ml-2 px-2"><strong> € 8.000,00</strong></span>
                                <a href="#" class="btn btn-warning">Vai all'annuncio</a>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="card rounded" style="width: 16rem;" >
                            <img class="img-fluid rounded" src="./img/moto.png" class="card-img-top" alt="">
                            <div class="card-body">
                                <h5 class="card-title d-inline-block">Honda Cbr</h5>
                                <span class="ml-3 px-2"><strong> € 1.500,00</strong></span>
                                <a href="#" class="btn btn-warning">Vai all'annuncio</a>
                            </div>
                        </div>   
                    </div>
                    
                    <div class="">
                        <div class="card rounded" style="width: 16rem;">
                            <img class="img-fluid rounded" src="./img/macbook.png" class="card-img-top" alt="">
                            <div class="card-body">
                                <h5 class="card-title d-inline-block">Macbook pro</h5>
                                <span class="ml-1 px-2"><strong> € 350,00</strong></span>
                                <a href="#" class="btn btn-warning">Vai all'annuncio</a>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>  
        </div> 
    </div>
</div>