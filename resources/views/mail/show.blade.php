<html>
    <body>
        <h1 style="color: #00FFA5">Optima.it</h1>
        <h5>Ti è arrivato un messaggio da: {{$bag['email']}}</h5>
        <h3><strong>Oggetto:</strong> <i>{{$bag['subject']}}</i></h3>
        <ul>
            <li> <strong>Nome:</strong> <i>{{$bag['name']}}</i>  </li>
            <li><strong>Email:</strong> <i>{{$bag['email']}}</i>  </li>
            <li><strong>Messaggio:</strong> <i>{{$bag['message']}}</i> </li>
        </ul>
    </body>
</html>
