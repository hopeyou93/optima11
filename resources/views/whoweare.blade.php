<x-layouts.app
title="Chi Siamo"
description="Benvenuti nella homepage del sito"
>

<header class="masthead2">

</header>

<div class="container">
    <div class="row">
        <div class="col-12 d-flex justify-content-center py-5">
            <img src="img/logoptima-removebg-preview.png">
        </div>
        <div class="col-6 py-3">
            <img src="/img/undraw_Content_structure_re_ebkv.png" data-aos="fade-right" class="img-fluid" style="height:500px; width:500px;">
        </div>
        <div class="col-6 py-5" data-aos="fade-up">
            <h2 class="text-center">Con Optima,<br> è facile <font color="#60E2E6">mettere insieme </font> i pezzi!</h2>
            <span>Hai dei vestiti che non indossi più? Niente paura!
                Optima è il nuovo servizio studiato per permetterti di fare ricavo dai vestiti non più usati, più semplice di cosí!</span>
                <img src="/img/undraw_Landscape_mode_re_r964.png" class="img-fluid" data-aos="fade-left" style="height:350px; width:610px;">

            </div>
        </div>
    </div>
</div>


<section id="call-to-action" data-aos="zoom-in" class="call-to-action" style="background:#0AB1BA">
    <div class="container" data-aos="zoom-out">
        <div class="row align-items-center">
            <div class="col-lg-9 py-5 text-center text-lg-left">
                <h3 class=""><font color="#fff">Il tuo primo annuncio</font></h3>
                <span class="d-flex align-items-center justify-content-center text-light"> Premendo il bottone 'Inizia a Vendere' verrai diretto nella pagina dove potrai inserire i dati dei tuoi vestiti. Non dimenticarti di mettere delle belle foto per aumentare le probabilità di vendita!</p>
                </div>
                <div class="col-lg-3 pb-5cta-btn-container text-center py-5 my-5 d-flex align-items-center">
                    <a class="btn btn-primary" href="/admin"><img src="/img/raviolo.png" class="mr-2" style="width:25px; height:25px;">Inizia a Vendere</a>
                </div>
            </div>

        </div>
    </section>
</div>



<div class="container">
    <div class="row">
        <div class="col-4 py-5 my-5" data-aos="zoom-in-left">
            <h2 >Sei un <font color="#00FFA5">acquirente</font> o un <font color="#00FFA5">venditore?</h2></font>
            <h5 class="text-muted">
                Comunque tu scelga di essere venditore o acquirente, sarai coperto dalla garanzia Optima, un tratto distintivo che si valorizza nel tempo lasciandoti senza pensieri.<br><br>

                Due diversi metodi di pagamento: tramite <font color="#00FFA5">Carta di Credito</font> o <font color="#00FFA5">Paypal</font>, cosí compri quello che <strong>vuoi</strong>, <strong>come</strong> vuoi. <3
            </h5>
        </div>
        <div class="col-8">
            <img src="/img/Offline_shop.png" class="img-fluid" data-aos="fade-up"
            style="width: 720px; height: 570px;">
        </div>
    </div>
</div>


<script>
    AOS.init();
</script>

</x-layouts.app>
