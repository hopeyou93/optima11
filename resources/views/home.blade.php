<x-layouts.app
title="Homepage"
description="Benvenuti nella homepage del sito"
>


<header class="masthead">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-3">
                <div class="ads">
                    <h1 class="py-4 px-5 text-dark">È arrivato il momento di liberare l'armadio!</h1>
                    <div class="input-group ">
                        <form class="form-inline">
                            <a class="button3 py-2 px-4 mx-5" href="/admin/resources/articles/new">Vendi<a></form>
                            </div>
                            <p class="text-main pl-5 py-3"><a href="#">Scopri come funziona</a></p>
                        </div>
                    </div>
                </div>
            </header>
            
            <div class="container my-5">
                <div class="row">
                    @foreach($articles as $article)
                    <div class="col-3">
                        
                        
                        <a href=" {{ route('userpage', $article->user) }}" class="text-secondary">
                            
                            
                            <img src="{{ Storage::url($article->user->avatar) }}" class="rounded-circle m-1 my-3" style="height:30px; width:30px">
                            {{ $article->user->name }}</a>
                            <a href="{{ $article->url() }}">
                                <img src="{{ $article->getFirstMediaUrl('gallery') }}" style="height:400px; width:250px">
                            </a>
                            <div class="py-2">
                                <span class="h5 card-title"><strong>{{ $article->prezzo }}€</strong></span><br>
                                <span class="h5 card-title text-secondary">{{  $article->marca }}</strong></span> <br>
                                <span class="h5 card-title text-secondary">{{ $article->taglia }}</span> <br>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    
                    
                    
                </div>
                
            </div>
            
            
            
        </x-layouts.app>
        