<x-layouts.app
title="Ricerca"
description="Benvenuti nella homepage del sito"
>

<div class="container my-5">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h3>Risultati per ricerca {{ $q }}</h3>
        </div>


        @foreach($articles as $article)
        <div class="col-3">
            <a href=" {{ route('userpage', $article->user) }}" class="text-secondary">


                <img src="{{ Storage::url($article->user->avatar) }}" class="rounded-circle m-1 my-3" style="height:30px; width:30px">
                {{ $article->user->name }}</a>
            <a href="{{ $article->url() }}">
                <img src="{{ $article->getFirstMediaUrl('gallery') }}" style="height:400px; width:250px">
            </a>
            <div class="py-2">
                <span class="h5 card-title"><strong>{{  $article->marca }}</strong></span> <br>
                <span class="h5 card-title">{{ $article->taglia }}</span> <br>
                <span class="h5 card-title">{{ $article->prezzo }}€</span>
            </div>
        </div>

        @endforeach

    </div>
</div>









</x-layouts.app>
