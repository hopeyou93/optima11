<link rel="stylesheet" href="/css/navbar.css" />
<div class="riz-top-header">
    <header class="header">
        <a href="{{ url('/') }}" class="logo">
            <img src="/img/optimauraki.png" class="ml-2 mr-2" alt="Logo Optima" style="height:40px; width:40px;">
            <img src="/img/logoptima-removebg-preview.png" style="height:40px; width:100px;" class="py-1">
        </a>
        <input class="menu-btn" type="checkbox" id="menu-btn" />
        <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>
        <div id="wrapper">
            <div class="custom-select-riz">
                <select id="option2">
                    <option value="catalog">Catalogo</option>
                    <option value="catalog">Catalogo</option>
                    <option value="members">Membri</option>
                    <option value="forum">Forum</option>
                    <option value="help">Help Desk</option>
                </select>
            </div>
            <form class="example" action=" {{ route('search') }} " method="GET">

{{--                 <livewire:search-dropdown>
 --}}
 <div>
    <input
    wire:model.debounce.300ms="search"
    type="text"
    placeholder="Cerca nel Catalogo"
    name="q"
    id="search1"
    />
</div>
                <button type="submit"><i class="fa fa-search"></i></button>
            </form>
        </div>

        <ul class="menu">
            <li>
                @guest
                @if (Route::has('login'))
                <span class="button button2 header-link" href="#about">
                    <a style="float:left; padding: 0; padding-right: 5px; color: #09b1ba" href="{{ route('login') }}">{{ __('Login') }}</a>&nbsp;
                    @endif
                    @if (Route::has('register'))
                    &nbsp; <a style="float:left; padding: 0; color: #09b1ba" href="{{ route('register') }}">| {{ __('Registrati') }}</a>
                    @endif
                    @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}
                        </a>


                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </li>
                @endguest
            </span>
        </li>
        <li><a class="button button3" href="/admin">Vendi Adesso</a></li>
        <li id="hide1">
            <a href="#contact">
                <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                style="fill: #999"
                viewBox="0 0 24 24"
                >
                <path
                d="M12.75 14.506h-1.5v-1.139c0-1.077.598-2.064 1.522-2.514a1.78 1.78 0 0 0 .927-2.068 1.741 1.741 0 0 0-1.218-1.218 1.766 1.766 0 0 0-1.559.305 1.75 1.75 0 0 0-.685 1.395h-1.5c0-1.017.46-1.958 1.266-2.58a3.285 3.285 0 0 1 2.853-.574 3.244 3.244 0 0 1 2.297 2.297c.398 1.542-.31 3.1-1.723 3.79-.413.202-.68.66-.68 1.167v1.139zM12 2c5.523 0 10 4.478 10 10 0 5.523-4.477 10-10 10-5.524 0-10-4.477-10-10C2 6.479 6.476 2 12 2zm0 1.5c-4.687 0-8.5 3.814-8.5 8.5 0 4.687 3.813 8.5 8.5 8.5 4.686 0 8.499-3.813 8.499-8.5 0-4.686-3.813-8.5-8.5-8.5H12zm-1.003 13.507c0-.5526.448-1.0005 1.0005-1.0005.5526 0 1.0005.448 1.0005 1.0005 0 .5523-.4477 1-1 1s-1-.4477-1-1h-.001z"
                ></path>
            </svg>
        </a>
    </li>
    <ul id="d1">
        <h3>Categories</h3>
        <li>
            <a href="#home" >
                <img src="img/img1.png" alt="" /><span>Donna</span>
            </a>
        </li>
        <li>
            <a href="#news">
                <img src="img/img2.png" alt="" /><span>Uomo</span>
            </a>
        </li>
        <li>
            <a href="#news">
                <img src="img/img1.png" alt="" /><span>Bambino</span>
            </a>
        </li>
        <li>
            <a href="#news" ><img src="img/img1.png" alt="" /><span>About</span>
            </a>
        </li>
    </ul>
</ul>
</header>
<ul id="d2">
    <h3>Categories</h3>
    <li class="dropdown">
        <a href="javascript:void(0)" class="dropbtn">Women</a>
        <div class="dropdown-content">
            <a href="#home"
            ><img src="/img/img2.png" alt="" style="height:20px; width:20px;" /><span>Clothes</span></a
            >
            <a href="#home"
            ><img src="/img/img1.png" alt="" style="height:20px; width:20px;"/><span>Shoes</span></a
            >
            <a href="#home"
            ><img src="/img/img3.png" alt="" style="height:20px; width:20px;"/><span>Accessories</span></a
            >
        </div>
    </li>
    <li class="dropdown">
        <a href="javascript:void(0)" class="dropbtn">Men</a>
        <div class="dropdown-content">
            <a href="#home"
            ><img src="/img/img2.png" alt="" style="height:20px; width:20px;"/><span>Clothes</span></a
            >
            <a href="#home"
            ><img src="/img/img1.png" alt="" style="height:20px; width:20px;"/><span>Shoes</span></a
            >
            <a href="#home"
            ><img src="/img/img3.png" alt="" style="height:20px; width:20px;"/><span>Accessories</span></a
            >
        </div>
    </li>
    <li class="dropdown">
        <a href="javascript:void(0)" class="dropbtn">Kids</a>
        <div class="dropdown-content">
            <a href="#home"
            ><img src="/img/img2.png" alt="" style="height:20px; width:20px;"/><span>Clothes</span></a
            >
            <a href="#home"
            ><img src="/img/img1.png" alt="" style="height:20px; width:20px;"/><span>Shoes</span></a
            >
            <a href="#home"
            ><img src="/img/img3.png" alt="" style="height:20px; width:20px;"/><span>Accessories</span></a
            >
        </div>
    </li>
    <li class="dropdown">
        <a href="javascript:void(0)" class="dropbtn">About</a>
        <div class="dropdown-content">
            <a href="{{ route('whoweare') }}"
            ><img src="/img/5.png" alt="" style="height:20px; width:20px;"/><span>Chi Siamo</span></a
            >
            <a href="#home"
            ><img src="/img/3.png" alt="" style="height:20px; width:20px;"/><span>Come Funziona</span></a
            >
            <a href="#home"
            ><img src="/img/4.png" alt="" style="height:20px; width:20px;"/><span>Privacy</span></a
            >
            <a href="{{ route('workus') }}"
            ><img src="/img/2.png" alt="" style="height:20px; width:20px;"/><span>Lavora Con Noi</span></a
            >
            <a href="#home"
            ><img src="/img/1.png" alt="" style="height:20px; width:20px;"/><span>Termini e Condizioni</span></a
            >
        </div>
    </li>
</ul>
</div>
<script src="/js/navbar.js"></script>





















{{-- <nav class="navbar navbar-expand-md sticky-top navbar-light bg-white shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="/img/optimauraki.png" class="ml-2 mr-2" alt="Logo Optima" style="height:40px; width:40px;">
            <img src="/img/logoptima-removebg-preview.png" style="height:40px; width:100px;" class="py-1">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto px-3">
                <li class="nav-item px-5">
                    <a class="nav-link" href="{{ route('home') }}">HOME</a>
                </li>
                <li class="nav-item px-5">
                    <a class="nav-link" href="#">ACQUISTA</a>
                </li>
                <li class="nav-item px-5"><a class="nav-link" href="#">VENDI</a></li>
                <li class="nav-item px-5"><a class="nav-link" href="#">FAQ</a></li>
                <li class="nav-item px-5"><a class="nav-link" href="#">CONTATTI</a></li>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                @if (Route::has('login'))
                <li class="nav-item">
                    <a class="nav-link px-4 circleScaleBtn" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @endif

                @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link px-4 ml-3 circleScaleBtn" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
                @endif
                @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }}
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
            </li>
            @endguest
        </ul>
    </div>
</div>
</nav> --}}










{{-- <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Navbar</title>
        <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
        />
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>
        <div class="container">
            <header class="header">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="img/optimauraki.png" class="ml-2 mr-2" alt="Logo Optima" style="height:40px; width:40px">
                    <img src="img/logoptima-removebg-preview.png" style="height:40px; width:100px" class="py-1">
                </a>
                <input class="menu-btn" type="checkbox" id="menu-btn" />
                <label class="menu-icon" for="menu-btn">
                    <span class="navicon">
                    </span>
                </label>
                <div id="wrapper">
                    <div class="custom-select">
                        <select id="option2">
                            <option value="catalog">Catalog</option>
                            <option value="catalog">Catalog</option>
                            <option value="members">Members</option>
                            <option value="forum">Forum</option>
                            <option value="help">Help Center</option>
                        </select>
                    </div>
                    <form class="example" action="">
                        <input
                        type="text"
                        placeholder="Search for items"
                        name="search2"
                        id="search1"
                        />
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>

                <ul class="menu">
                    @guest
                    @if (Route::has('login'))
                    <li class="nav-item">
                        <a class="nav-link px-4 circleScaleBtn" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @endif

                    @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link px-4 ml-3 circleScaleBtn" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                    @endif
                    @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </li>
                @endguest

                <li id="hide1">
                    <a href="#contact">
                        <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        style="fill: #999"
                        viewBox="0 0 24 24"
                        >
                        <path
                        d="M12.75 14.506h-1.5v-1.139c0-1.077.598-2.064 1.522-2.514a1.78 1.78 0 0 0 .927-2.068 1.741 1.741 0 0 0-1.218-1.218 1.766 1.766 0 0 0-1.559.305 1.75 1.75 0 0 0-.685 1.395h-1.5c0-1.017.46-1.958 1.266-2.58a3.285 3.285 0 0 1 2.853-.574 3.244 3.244 0 0 1 2.297 2.297c.398 1.542-.31 3.1-1.723 3.79-.413.202-.68.66-.68 1.167v1.139zM12 2c5.523 0 10 4.478 10 10 0 5.523-4.477 10-10 10-5.524 0-10-4.477-10-10C2 6.479 6.476 2 12 2zm0 1.5c-4.687 0-8.5 3.814-8.5 8.5 0 4.687 3.813 8.5 8.5 8.5 4.686 0 8.499-3.813 8.499-8.5 0-4.686-3.813-8.5-8.5-8.5H12zm-1.003 13.507c0-.5526.448-1.0005 1.0005-1.0005.5526 0 1.0005.448 1.0005 1.0005 0 .5523-.4477 1-1 1s-1-.4477-1-1h-.001z"
                        ></path>
                    </svg>
                </a>
            </li>
        </ul>
    </header>
</div>

<script>
    var x, i, j, l, ll, selElmnt, a, b, c;
    /*look for any elements with the class "custom-select":*/
    x = document.getElementsByClassName("custom-select");
    l = x.length;
    for (i = 0; i < l; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        ll = selElmnt.length;
        /*for each element, create a new DIV that will act as the selected item:*/
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        /*for each element, create a new DIV that will contain the option list:*/
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 1; j < ll; j++) {
            /*for each option in the original select element,
            create a new DIV that will act as an option item:*/
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function (e) {
                /*when an item is clicked, update the original select box,
                and the selected item:*/
                var y, i, k, s, h, sl, yl;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                sl = s.length;
                h = this.parentNode.previousSibling;
                for (i = 0; i < sl; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        yl = y.length;
                        for (k = 0; k < yl; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        break;
                    }
                }
                h.click();
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);
        a.addEventListener("click", function (e) {
            /*when the select box is clicked, close any other select boxes,
            and open/close the current select box:*/
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
        });
    }
    function closeAllSelect(elmnt) {
        /*a function that will close all select boxes in the document,
        except the current select box:*/
        var x,
        y,
        i,
        xl,
        yl,
        arrNo = [];
        x = document.getElementsByClassName("select-items");
        y = document.getElementsByClassName("select-selected");
        xl = x.length;
        yl = y.length;
        for (i = 0; i < yl; i++) {
            if (elmnt == y[i]) {
                arrNo.push(i);
            } else {
                y[i].classList.remove("select-arrow-active");
            }
        }
        for (i = 0; i < xl; i++) {
            if (arrNo.indexOf(i)) {
                x[i].classList.add("select-hide");
            }
        }
    }
    /*if the user clicks anywhere outside the select box,
    then close all select boxes:*/
    document.addEventListener("click", closeAllSelect);

    //  function changePlaceholder123() {
        //   console.log("haiii");
        //   /*let option1 = document.getElementById("option");
        //   let search1 = document.getElementById("search1");
        //   console.log(option1.value);
        //   console.log(option1.value);
        //   option1.on("change", function () {
            //     empSearch.attr(
            //       "placeholder",
            //       "Search " + select_designatfirst.find(":selected").text()
            //     );
            //   });*/
            // }
        </script> --}}
    </body>
    </html>
