<div class="container-fluid bg-light">
    <div class="row bg-main py-5 justify-content-center">
        <div class="col-12 col-sm-12 d-flex justify-content-center">
            <img src="/img/optimauraki.png" width="65px" height="65px">
            <img src="/img/logoptima-removebg-preview.png" style="height:40px; width:100px;" class="img-fluid my-3 pl-2">

        </div>
        <div class="col-12 col-lg-12 p-3 d-flex justify-content-center">

            <div class="sm">
                <a href="https://www.facebook.com/"><i class="fab fa-facebook-f"></i></a>
                <a href="https://twitter.com/"><i class="fab fa-twitter"></i></a>
                <a href="https://www.instagram.com/"><i class="fab fa-instagram"></i></a>
                <a href="https://www.youtube.com/"><i class="fab fa-youtube"></i></a>
                <a href="#"><i class="fab fa-linkedin-in"></i></a>
                <a href="#"><i class="fab fa-behance"></i></a>
            </div>
        </div>
        <div class="col-12 d-flex justify-content-center">
            <h4 class="text-main">All right reserved - Copyright 2020 - Optima s.r.l.</h4>
        </div>

    </div>


</div>

</div>
</div>
