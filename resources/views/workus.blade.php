<x-layouts.app
title="Chi Siamo"
description="Benvenuti nella homepage del sito"
>

<div class="container">
    <div class="row">
        <div class="col-12 py-5 my-5">
            <h1 class="text-center">Lavora con Noi</h1>
        </div>
        <div class="col-5">
            <h1>Come posso <font color="#60E2E6">lavorare per Optima?</h1></font>
            <span class="text-muted">Attualmente Optima ricerca dei revisori da poter aggiungere al suo team una risorsa che risulti in minori tempi di attesa per la corretta pubblicazione degli articoli.
            Se hai tempo libero o vuoi dedicarti a un qualcosa di molto semplice puoi richiedere qui nel form di seguito l'abilitazione dell'account revisore.
         </span>
        </div>
        <div class="col-7">
            <div class="container-fluid my-3">
                <div class="row justify-content-center">

                    @if (\Session::has('message'))
                    <div class="alert alert-success">
                        <ul class="list-unstyled text-center">
                            <li>{!! \Session::get('message') !!}</li>
                        </ul>
                    </div>
                    @endif
                        <form method="POST" action="{{route('contact.send')}}" id="contactForm" name="contact-form">
                          @csrf
                            <div class="form-group">
                              <label for="name" class="text-accent font-weight-bold">Nome</label>
                              <input type="string" name="name" class="form-control"   placeholder="Inserisci il tuo Nome" value="{{old('name')}}" required>
                              @error('name')
                              <div class="alert alert-danger">{{ $message }}</div>
                              @enderror
                            </div>
                            <div class="form-group">
                                <label for="subject" class="text-accent font-weight-bold">Oggetto</label>
                                <input type="string" name="subject" class="form-control"   placeholder="Inserisci l'Oggetto" value="{{old('subject')}}" required>
                                @error('subject')
                              <div class="alert alert-danger">{{ $message }}</div>
                              @enderror
                              </div>
                            <div class="form-group">
                              <label for="massage" class="text-accent font-weight-bold">Descrizione</label>
                              <textarea class="form-control" placeholder="Inserisci qui il tuo Messaggio" name="message" type="text" cols="60" rows="10" required>{{old('message')}}</textarea>
                              @error('message')
                              <div class="alert alert-danger">{{ $message }}</div>
                              @enderror
                            </div>

                              <div class="form-group">
                                <label for="email" class="text-accent font-weight-bold">Email</label>
                                <input type="email" name="email" class="form-control"  placeholder="Inseirisci il tuo indirizzo Email" required>
                                @error('email')
                              <div class="alert alert-danger">{{ $message }}</div>
                              @enderror
                              </div>

                            <button type="submit" class="btn btn-warning text-dark font-weight-bold">Invia</button>
                          </form>
                    </div>
                </div>
            </div>




            </form>
        </div>
    </div>
</div>






</x-layouts.app>
