<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;
use Faker\Provider\Image;
use Faker\Provider\Lorem;

$factory->define(Post::class, function (Faker $faker) {
    $faker->addProvider(new Image($faker));
    $faker->addProvider(new Lorem($faker));
    return [
        'url' => this->faker->imageUrl(640, 480),
        'preview' => $this->faker->imageUrl(320, 240),
        'text' => $this->faker->sentence(10, true)
    ];
});


