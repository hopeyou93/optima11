<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Faker\Provider\Internet;
use Faker\Provider\Image;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

public function definition() {
    $faker->addProvider(new Internet($faker));
    $faker->addProvider(new Image($faker));
    return [
        'name' => $faker->name,
        'avatar' => $faker->imageUrl(640, 480),
        'email' => $faker->unique()->safeEmail,
        'password' => $faker->password,
        'remember_token' => Str::random(10),
    ];
});
