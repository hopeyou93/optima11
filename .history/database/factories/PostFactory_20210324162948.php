<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;
use Faker\Provider\Image;
use Faker\Provider\Lorem;

Œ
    return [
        'url' => $faker->imageUrl(640, 480),
        'preview' => $faker->imageUrl(320, 240),
        'text' => $faker->sentence(10, true)


    ];
});


