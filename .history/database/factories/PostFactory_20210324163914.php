<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Provider\Image;
use Faker\Provider\Lorem;
use Faker\Generator as Faker;
use Laravel\Nova\Tests\Fixtures\Post;

class UserFactory extends Factory
{

    protected $model = Post::class;


$factory->define(Post::class, function (Faker $faker) {
    $faker->addProvider(new Image($faker));
    $faker->addProvider(new Lorem($faker));
    return [
        'url' => $this->faker->imageUrl(640, 480),
        'preview' => $this->faker->imageUrl(320, 240),
        'text' => $this->faker->sentence(10, true)
    ];
});
]


