<?php

namespace Database\Factories;

use App\Models\Article;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArticleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Article::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker->addProvider(new Image($faker));
       $faker->addProvider(new Lorem($faker));
        return [
            'url' => $this->faker->imageUrl(640, 480),
            'preview' => $this->faker->imageUrl(320, 240),
            'text' => $this->faker->sentence(10, true)
        ];
    }
}
