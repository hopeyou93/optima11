<?php

namespace App\Database\Factories\;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Factory;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
class UserFactory extends Factory
{

protected $model = User::class;


 public function definition() {

    return [
        'name' => $this->faker->name,
        'avatar' => $this->faker->imageUrl(640, 480),
        'email' => $this->faker->unique()->safeEmail,
        'password' => $this->faker->password,
        'remember_token' => Str::random(10),
    ];
}
}
