<?php

namespace Database\Factories;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Factory;
use Illuminate\Support\Facades\Hash;

use Faker\Generator as Faker;

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});



/* class UserFactory extends Factory
{

protected $model = User::class;


 public function definition() {

    return [
        'name' => $this->faker->name,
        'avatar' => $this->faker->imageUrl(640, 480),
        'email' => $this->faker->unique()->safeEmail,
        'password' => $this->faker->password,
        'remember_token' => Str::random(10),
    ];
}
} */


