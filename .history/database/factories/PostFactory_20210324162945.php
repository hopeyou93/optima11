<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;
use Faker\Provider\Image;
use Faker\Provider\Lorem;

Œ
    return [
        'url' => $faker->imageUrl(640, 480),
        'preview' => $faker->imageUrl(320, 240),
        'text' => $faker->sentence(10, true)

        return [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ];


    ];
});


