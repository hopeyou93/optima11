<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Provider\Image;
use Illuminate\Support\Str;
use Faker\Provider\Internet;
use Faker\Generator as Faker;
use Illuminate\Foundation\Auth\User;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

protected $model = User::class;


 public function definition() {
    $faker->addProvider(new Internet($faker));
    $faker->addProvider(new Image($faker));
    return [
        'name' => $faker->name,
        'avatar' => $faker->imageUrl(640, 480),
        'email' => $faker->unique()->safeEmail,
        'password' => $faker->password,
        'remember_token' => Str::random(10),
    ];
});
