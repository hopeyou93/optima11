<?php

namespace Database\Factories;

use App\Models\Article;
use Faker\Provider\Image;
use Faker\Provider\Lorem;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArticleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Article::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $this->faker->addProvider(new Image($this->faker));
        $this->faker->addProvider(new Lorem($this->faker));
        return [
            'img' => $this->faker->imageUrl(640, 480),
            'title' => $this->faker->sentence(10, true),
            'prezzo' => $this->faker->random_i(1, 100),
        ];
    }
}
