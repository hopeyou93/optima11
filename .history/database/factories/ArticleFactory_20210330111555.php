<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Provider\Image;
use Faker\Provider\Lorem;
use Faker\Generator as Faker;
use Laravel\Nova\Tests\Fixtures\Post;

class ArticleFactory extends Factory
{

    protected $model = Article::class;


    public function definition() {
   /*  $faker->addProvider(new Image($faker));
    $faker->addProvider(new Lorem($faker)); */
    return [
        'url' => $this->faker->imageUrl(640, 480),
        'preview' => $this->faker->imageUrl(320, 240),
        'text' => $this->faker->sentence(10, true)
    ];
});
}


