<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Model\Article as ArticleModel;

class Article extends Seeder
{


    public function run()
    {
        ArticleModel::factory()
            ->count(50)
            ->create();
        }
}
