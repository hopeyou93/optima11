<?php


use Illuminate\Database\Seeder;
use App\Models\Article as ArticleModel;

class Article extends Seeder
{


    public function run()
    {
        factory(Article::class, 50)->create();

}
}
