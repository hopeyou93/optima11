<?php

namespace Database\Seeders;

use App\Models\Article;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->count(50)->hasArticles(5)->create();


    }
}
