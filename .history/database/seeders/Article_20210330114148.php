<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Article as ArticleModel;

class Article extends Seeder
{


    public function run()
    {
        factory(App\Article::class, 50)->create();

}
