<?php

namespace App\Models;

use App\Models\Article;
use App\Models\Favourites;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'role',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];





    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


        public function favourites()
        {
            return $this->hasMany(Favourites::class);
        }

    // Metodo custom che restituisce true / false se l'utente è amministratore o meno
    public function isAdmin()
    {
        return $this->role == 'admin';
    }


    // Metodi utilizzati da Nova Impersonate
    public function canImpersonate()
    {
        return $this->isAdmin();
    }

    public function canBeImpersonated()
    {
        return $this->isAdmin();
    }


    // Relazioni del modello
    public function articles()
    {
        return $this->hasMany(Article::class);
    }


}
