<?php

namespace App\Models;

use Spatie\Tags\HasTags;
use App\Models\Favourites;
use App\Models\ArticleImage;
use Laravel\Scout\Searchable;

use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Article extends Model implements HasMedia
{
    use Searchable;


    protected $fillable = ['title','description','marca'];

    public function toSearchableArray()
    {
        $array = [
            'id'=> $this->id,
            'title'=> $this->title,
            'description'=> $this->description,
            'marca'=> $this->marca,
            'altro' => 'vestiti',
        ];

            return $array;
    }


    use HasFactory, InteractsWithMedia, HasTags;

    protected $guarded = [];

    // Link rapido. Chiamando il metodo url di una risorsa, otteniamo la sua rotta parametrica, già popolata con il suo id
    public function url()
    {
        return route('article', [$this->id, \Str::slug($this->title)]);
    }


    public function favourite()
    {
        return $this->belongsTo(Favourites::class, 'id', 'article_id');
    }


    // Anteprima (rifattorizzato in 2 metodi)
    // Il metodo preview prende un parametro in entrata. Rimuove i tag html. Taglia i primi 90 caratteri. Aggiunge '...'
    public function preview($anteprima) {
        $anteprima = strip_tags($anteprima);
        $anteprima = substr($anteprima, 0 , 90);
        $anteprima = $anteprima . "...";
        return $anteprima;
    }
    // Il metodo getPreview chiama il metodo preview passando di volta in volta il campo di desrizione che vogliamo accorciare
    public function getPreview() {
        return $this->preview($this->description);
    }


    // Spatie Media Library (Media Collections e Media Conversions)
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('gallery')
            ->useFallbackUrl('/img/immagine.jpg');
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->crop(Manipulations::CROP_CENTER, 720, 405)
            ->sharpen(8)
            ->performOnCollections('gallery');
    }

    // Relazioni del modello
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function ArticleImages()
    {
        return $this->hasMany(ArticleImage::class);
    }

}

