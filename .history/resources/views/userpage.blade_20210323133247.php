<x-layouts.app
title="Pagina Utente"
description="Benvenuti nella homepage del sito"
>

<br>
<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-md-3 py-5 ml-2">
            <div style="height:1090px; width:310px; background-color:white;" class="text-center bg-light border bulla">
                <img src="{{ Storage::url($users->avatar) }}" style="max-width: 100%;
                height: auto;" class="img-fluid" />
                <h3 class="font-weight-bold py-3 text-muted text-center font-weight-bold"><strong>{{ $users->name }}</strong></h3>
                <hr>
                <p class="font-weight-bold mx-3 py-2 pb-2 text-muted">{{ $users->description }}</p>
                <hr>




                <div class="py-2">
                    <p class="text-dark h6">Verified Info</p>
                    <i><img src="/img/icons8-assessments-96.png" style="width:24px; height:24px;"> <span class="">Facebook
                    </span></i><br>
                    <i><img src="/img/icons8-assessments-96.png" style="width:24px; height:24px;" class="mb-2"> <span class="pr-0">Email</span></i>
<hr>
                    <div class="col-12 py-4">
                    <a class="button3 py-2 px-4 mx-5 my-5" href="/">Contattami</a><br><br>
                    <a class="button3 py-2 px-4 mx-5" href="/"><i class="fas fa-rss-square"></i> Follow Me</a>
                </div>

                </div>

            </div>
        </div>

        <div class="col-sm-8">
            <div class="row">

                @foreach ($articles as $article)
                <div class="col-12 col-md-4 text-center ml-0 py-5 border border-grey">
                    <a href="{{ $article->url() }}">
                        <h5 class="text-dark font-weight-bold">{{ $article->title }}</h5>
                        <img class="img-fluid" src="{{ $article->getFirstMediaUrl('gallery') }}" style="width:280px; height:400px;">
                    </a>
                    <div class="py-0 border border" style="text-align: justify;">
                        <span class="h5 card-title"><strong>{{ $article->prezzo }}€</strong></span><br>
                        <span class="h5 card-title bulla">{{ $article->taglia }}</span> <br>
                        <span class="h5 card-title bulla">{{  $article->marca }}</strong></span> <br>
                    </div>

                </div>
                @endforeach
            </div>
        </div>

    </div>
</div>
</x-layouts.app>

