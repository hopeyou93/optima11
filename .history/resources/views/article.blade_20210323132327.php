<x-layouts.app
title="{{ $article->title }}"
description="{{ $article->getPreview() }}"
>

@push('styles')

<link href="{{$article->url()}}" rel="canonical"/>
<script src="node_modules/@glidejs/glide/dist/glide.min.js"></script>
<link rel="stylesheet" href="css/glide.core.min.css">
<link rel="stylesheet" href="css/glide.theme.min.css">



<style>

</style>
@endpush

<div class="container">

    <header class="row py-5">
        <div class="col-12 ">

            {{-- <figure>
                <img src="{{ Storage::url($article->img) }}" alt="{{ $article->title }}" class="ml-5" style="width:450px; height:300px;">
            </figure> --}}

        </div>

        <div class="col-6 text-center">
            <h1>{{ $article->title }}</h1>
            <div class="glide py-5">
                <div class="glide__track" data-glide-el="track">
                    <ul class="glide__slides">
                        @if (basename($article->getMedia('gallery')) == "[]")
                            <li class="glide__slide"><img class="doren" src="/img/immagine.jpg" style="width:350px; height:490px;">
                        @else
                            @foreach ($article->getMedia('gallery') as $img)
                                <li class="glide__slide"><img class="doren" src="{{ $img->getUrl('') }}" style="width:350px; height:490px;">
                            @endforeach
                        @endif
                    </ul>
                </div>

                <div class="glide__arrows" data-glide-el="controls">
                    <button class='glide__arrow glide__arrow-- bg-primary' data-glide-dir="<">Indietro</button>
                    <button class="glide__arrow glide__arrow--right bg-primary" data-glide-dir=">">Avanti</button>
                </div>
            </div>
        </div>
        {{-- <p>
            @foreach($article->tags as $tag)
            <span class="badge badge-warning px-2 py-1 mr-1">{{ $tag->name }}</span>
            @endforeach
        </p> --}}

        {{-- <figure>
            <img src="{{ Storage::url($article->img) }}" alt="{{ $article->title }}" class="ml-5" style="width:300px; height:475px;">
        </figure>
    </div> --}}


    {{-- @foreach($article->getMedia('gallery') as $item)
    <figure>
        <img src="{{ $item->getUrl('thumb') }}" alt="{{ $article->title }}"  class="pl-3" width="100%">
    </figure>
    @endforeach --}}

    {{-- <figure class="mt-n3 py-4">
        @foreach($article->getMedia('gallery') as $item)

        <img src="{{ $item->getUrl('thumb') }}" alt="{{ $article->title }}"   style="width:330px; height:260px;">
    </figure>
    @endforeach --}}

    <div class="col-3 my-3 my-5 ml-5 bg-light" style="height:670px; background:grey">

        <div class="container-fluid">
            <div class="row py-2">
                <div class="col-12 py-3 ">
                    <h3> {{ $article->prezzo }}€</h3>
                </div>

                <div class="col-md-2 col-sm-12">
                    <div class="item-align-center height: 100px;">
                        <span style="color: #60E2E6; height:1000px" class="align-middle"><br><i class="fas fa-shield-alt fa-2x"></i>
                        </span>

                    </div>
                </div>
                <div class="col-10">
                    <span class="sensei">Sei coperto dalla garanzia Optima,
                        qualora andasse male, puoi contare sul rimborso.
                    </span>
                    <hr>
                </div>
                <hr><hr>
                <div class="col-6 py-0">

                    <h6>MARCA</h6>
                    <h6>TAGLIA</h6>
                    <h6>CONDIZIONI</h6>
                    <h6>COLORE</h6>
                    <h6>LOCAZIONE</h6>
                    <h6>PAGAMENTO</h6>
                    <h6 class="py-3">VISTO</h6>
                    <h6 class="py-1">ONLINE DA</h6>

                </div>
                <div class="col-6 py-0">
                    <h6 style="text-transform: uppercase;">{{ $article->marca }}</h6>
                    <h6 style="text-transform: uppercase;">{{ $article->taglia }}</h6>
                    <h6 style="text-transform: uppercase;">{{ $article->condizioni }}</h6>
                    <h6 style="text-transform: uppercase;">{{ $article->colore }}</h6>
                    <h6 style="text-transform: uppercase;">{{ $article->locazione }}</h6>
                    <h6 class="pb-1" style="text-transform: uppercase;">{{ $article->pagamento }}</h6>
                    {{--   <h6 class="pb-3">732 VOLTE</h6>
                    <h6>2 SETTIMANE</h6> --}}
                </div>
                <div class="col-12 py-1 d-flex justify-content-center">
                    <button type="submit" class="btn buttunxl btn-primary">
                        <i class="far fa-credit-card mr-3"></i>Compra Ora
                    </button>
                </div>
                <div class="col-lg-12 py-1 d-flex justify-content-center">
                    <button type="submit" class="btn buttunxl btn-primary">
                        <i class="fas fa-inbox mr-3"></i>Invia un Messaggio
                    </button>
                </div>

                {{-- SE L'UTENTE E' LOGGATO --}}
                {{-- {{dd($favourites->user_id)}} --}}
                @guest
                    <div class="col-lg-12 ml-2 py-1 d-flex justify-content-center">
                        <a href="{{route('login')}}" class="btn buttunxl btn-primary">
                            <i class="far fa-heart mr-2"></i>  Aggiungi ai Preferiti
                            <span class="badge badge-pill badge-warning">{{count($favCount)}}</span>
                        </a>
                    </div>
                @endguest

                {{-- SE LOGGATO --}}
                @auth
                    {{-- SE ESISTE UN FAVORITE --}}
                    @if ($favourites != null)
                        {{-- SE LA COLONNA USER ID COMBACIA CON L'ID DEL UTENTE LOGGATO --}}
                        @if ($favourites->user_id == Auth::user()->id)
                            <form action="{{ route('delete', $favourites) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <div class="col-lg-12 ml-2 py-1 d-flex justify-content-center">
                                    <button type="submit" title="Hai gia' messo mi piace!" class="btn buttunxl btn-danger">
                                        <i class="far fa-heart mr-2"></i> I Love It!
                                        <span class="badge badge-pill badge-warning">{{count($favCount)}}</span>
                                    </button>
                                </div>
                            </form>
                        @endif
                    @else
                        <form action="{{ route('favourites', $article->id) }}" method="POST">
                            @csrf
                            <div class="col-lg-12 ml-2 py-1 d-flex justify-content-center">
                                <button type="submit" class="btn buttunxl btn-primary">
                                    <i class="far fa-heart mr-2"></i>  Aggiungi ai Preferiti
                                    <span class="badge badge-pill badge-warning">{{count($favCount)}}</span>
                                </button>
                            </div>
                        </form>
                    @endif
                @endauth

                <div class="col-12 py-4">
                    <a href="{{ route('userpage', $article->user) }}">
                        <img src="{{ Storage::url($article->user->avatar) }}" alt="" class="rounded-circle" style="height:23px; width:23px"><span class="px-2 border-bottom-1"><strong>{{ $article->user->name}}</span></strong>
                    </a>
                    <hr>
                    <img src="/img/location.png" alt="" style="height:23px; width:23px" class="my-2"> <span class="my-5  ml-2">{{ $article->locazione }}</span>
                </div>

            </div>
        </div>
    </div>
</div>
<hr>



</div>
</div>
</div>



</header>


<div class="container my-5">
    <div class="row">
        @foreach($relatedarticles as $related)
        <div class="col-3 ">
            <div class="">
                <a href=" {{ $rearticle->user }}" class="text-secondary">
                    <img src="{{ Storage::url($article->user->avatar) }}" class="rounded-circle m-3 my-3" style="height:30px; width:30px">
                    {{ $article->user->name }}</a>
                    <a href="#"><p>{{ $related->autore }}</p></a>

                    <a href="{{ $related->url() }}">
                        <img src="{{ $related->getFirstMediaUrl('gallery') }}" alt="{{ $related->title }}" style="height:410px; width:270px">
                    </a>
                    <div class="py-2">
                        <span class="h5 card-title"><strong>{{ $related->prezzo }}€</strong></span><br>
                        <span class="h5 card-title text-secondary">{{  $related->marca }}</strong></span> <br>
                        <span class="h5 card-title text-secondary">{{ $related->taglia }}</span> <br>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>



</div>

<script src="https://cdn.jsdelivr.net/npm/@glidejs/glide"></script>

<script>
    const config = {
        type: "carousel",
        perView: 1,
        breakpotins: {
            800: {
                perView: 1
            },
            600: {
                perView: 1
            }
        }
    };
    new Glide('.glide', config).mount();
</script>

@push('scripts')
<style>

</style>
@endpush

</x-layouts.app>
