<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\FrontendController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontendController::class, 'index'])->name('home');

Route::get('/articles', [FrontendController::class, 'blogNews'])->name('articles');

Route::get('/notizia/{article_id}/{article_title?}', [FrontendController::class, 'article'])->name('article');

Route::get('userpage/{users}', [FrontendController::class, 'userpage'])->name('userpage');

Route::get('/search', [FrontendController::class, 'search'])->name('search');

Route::get('/whoweare', [FrontendController::class, 'whoweare'])->name('whoweare');

Route::get('/workus', [FrontendController::class, 'workus'])->name('workus');

Route::post('/contact-form', [ContactController::class, 'contact'])->name('contact.send');

Route::post('/favourites/{article}', [FrontendController::class, 'favourites'])->name('favourites');

Route::delete('/delete/{favourites?}', [FrontendController::class, 'removeFavourites'])->name('delete');

Auth::routes();